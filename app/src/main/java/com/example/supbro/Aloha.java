package com.example.supbro;

/**
 * Created by Thiago on 3/31/2017.
 */

public class Aloha {

    /**
     * Indicates latitude
     */
    @com.google.gson.annotations.SerializedName("geolat")
    private Number mGeolat;

    /**
     * Item Id
     */
    @com.google.gson.annotations.SerializedName("id")
    private String mId;

    /**
     * Indicates longitude
     */
    @com.google.gson.annotations.SerializedName("geolong")
    private Number mGeolong;

    /**
     * ToDoItem constructor
     */
    public Aloha() {

    }

    public Aloha(Number geolat, Number geolong, String id) {
        this.setLat(geolat);
        this.setId(id);
        this.setLong(geolong);
    }

    /**
     * Sets the item geolat
     */
    public final void setLat(Number geolat) {
        mGeolat = geolat;
    }

    /**
     * Sets the item geolong
     */
    public final void setLong(Number geolong) {
        mGeolong = geolong;
    }

    /**
     * Sets the item id
     */
    public final void setId(String id) {
        mId = id;
    }

    /**
     * Returns the item id
     */
    public String getId() {
        return mId;
    }

    /**
     * Returns latitude
     */
    public Number getLat() {
        return mGeolat;
    }

    /**
     * Returns latitude
     */
    public Number getLong() {
        return mGeolong;
    }

}
